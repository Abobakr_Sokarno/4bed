<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Product;

class Category extends Model
{
    protected $fillable = ['title_ar', 'title_en'];

    public function products()
    {
        return $this->hasMany(Product::class);
    }
}
