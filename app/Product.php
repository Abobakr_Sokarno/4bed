<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Category;
use App\Items;

class Product extends Model
{
     protected $fillable = ['title)_en', 'title_ar', 'describtion_en', 'describtion_ar','price', 'category_id', 'img_url' ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function items()
    {
        return $this->hasMany(Items::class);
    }
}
