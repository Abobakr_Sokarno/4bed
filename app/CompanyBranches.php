<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyBranches extends Model
{
    protected $fillable = ['address_en', 'address_ar', 'phone_one', 'phone_two'];

}
