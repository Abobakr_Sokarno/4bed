<?php

namespace App\Http\Controllers\ADMIN;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;

class CategoriesController extends Controller
{
    public function index()
    {
        $categories = Category::all();

        return view('dashboard.categories', compact('categories'));
    }

    public function show_add_form()
    {
    	return view('dashboard.addCategory');
    }

    public function show($id)
    {
        $category = Category::find($id);
        return view('dashboard.editCategory', compact('category'));
    }

    public function store(Request $request)
    {	
    	$this->validate(request(),[
            'title_en'=>'required',
            'title_ar'=>'required',
        ]);

        $category = Category::create($request->all());
        session()->flash('message', 'Category is Created');

        return redirect('createCategory');
    }

    public function update(Request $request, $id)
    {
    	$this->validate(request(),[
            'title_en'=>'required',
            'title_ar'=>'required',
        ]);

        $category=new Category;
        $category->where('id', $id)
        	 	  ->update(['title_ar'=>request('title_ar'), 'title_en'=>request('title_en')]);
       	session()->flash('message', 'Category is Updated');

        return redirect('/categories');
    }

    public function delete($id)
    {
        $category = Category::findOrFail($id);
        $category->delete();

        session()->flash('message', 'Category is Deleted');

        return back();
    }
}
