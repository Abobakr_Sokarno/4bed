<?php

namespace App\Http\Controllers\ADMIN;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Items;

class ItemsController extends Controller
{
    public function index()
    {
        $items = Items::all();

        return view('dashboard.items', compact('items'));
    }

    public function show_add_form()
    {
    	$products=Product::all();
    	return view('dashboard.addItem', compact('products'));
    }

    public function show($id)
    {
    	$products=Product::all();
        $item = Items::find($id);
        return view('dashboard.editItem', compact('item', 'products'));
    }

    public function store(Request $request)
    {	
    	$this->validate(request(),[
            'title_ar'=>'required',
            'title_en'=>'required',
            'describtion_ar'=>'required',
            'describtion_en'=>'required',
            'img_url'=>'required|image|mimes:jpg,jpeg,png,gif|max:2048',
            'product_id'=>'required', 
            'price'=>'required'
        ]);

        $img_name=time() . '.' . $request->img_url->getClientOriginalExtension();

        $item= new Items;
        $item->title_ar=request('title_ar');
        $item->title_en=request('title_en');
        $item->describtion_ar=request('describtion_ar');
        $item->describtion_en=request('describtion_en');
        $item->img_url=$img_name;
        $item->product_id=request('product_id');
        $item->price=request('price');
        $item->save();

        $request->img_url->move(public_path('uploads'), $img_name);

        session()->flash('message', 'Item is Created');

        return redirect('createItem');
    }

    public function update(Request $request, $id)
    {
    	$this->validate(request(),[
            'title_ar'=>'required',
            'title_en'=>'required',
            'describtion_ar'=>'required',
            'describtion_en'=>'required',
            'product_id'=>'required',
            'price'=>'required'
        ]);

        $item=new Items;
        $item->where('id', $id)
        	 	  ->update(['title_ar'=>request('title_ar'), 'title_en'=>request('title_en'), 'describtion_ar'=>request('describtion_ar'), 'describtion_en'=>request('describtion_en'), 'product_id' => request('product_id'), 'price'=>request('price')]);

       	session()->flash('message', 'Item is Updated');

        return redirect('/items');
    }

    public function delete($id)
    {
        $item = Items::findOrFail($id);
        $item->delete();

        session()->flash('message', 'Item is Deleted');

        return back();
    }
}
