<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Product;
use App\Items;
use App\ContactUs;
use App\CompanyBranches;

class APIcontroller extends Controller
{
    public function about_us(Request $request)
    {
        $lang = "ar" ; 
        if ($request['lang'] == 'en') 
            $lang = "en" ; 
            
        $about_us = DB::table('about')->value('about_'.$lang.' AS about');
        return response()->json(['result'=>$about_us]) ;
    }

    public function get_all_categories(Request $request)
    { 
        if (isset($request['lang']) == 'en') 
        {
            $categories = DB::table('categories')->get(['id','title_en AS title']);
        }else{
            $categories = DB::table('categories')->get(['id','title_ar AS title']);
        } 
        return response()->json(["result"=>$categories]); 
    }

    public function get_products_by_category(Request $request, $category_id)
    { 
        $lang = "ar" ; 
        if ($request['lang'] == 'en') 
            $lang = "en" ;
        
        $products = Product::where('category_id', $category_id)->get(['title_'.$lang.' AS title','describtion_'.$lang.' AS description','img_url']);
        return response()->json(["result"=>$products]) ;
    }

    public function get_items_by_product (Request $request, $product_id)
    {
        $lang = "ar" ; 
        if ($request['lang'] == 'en') 
            $lang = "en" ;
        
        $items = Items::where('product_id', $product_id)
            ->join('products','items.product_id','=','products.id')
            ->get(['items.title_'.$lang.' AS item_title','items.describtion_'.$lang,'items.img_url','items.price','products.title_'.$lang.' AS product_title']);
        return response()->json(["result"=>$items]) ;
    }

    public function get_all_products (Request $request)
    {
        $lang = "ar" ; 
        if ($request['lang'] == 'en') 
            $lang = "en" ;
        
        $products = Product::get(['title_'.$lang.' AS title','describtion_'.$lang.' AS description','img_url','price']);        
        return response()->json(['result'=>$products]) ;
    }

    public function get_product (Request $request, $product_id)
    {
        $lang = "ar" ; 
        if ($request['lang'] == 'en') 
            $lang = "en" ;
        
        $product = Product::where('id', $product_id)
            ->get(['title_'.$lang.' AS item_title',
            'describtion_'.$lang,
            'img_url',
            'price',
            'title_'.$lang.' AS product_title']);
        return response()->json(["result"=>$product]) ;
    }


    public function get_all_items(Request $request)
    {
        $lang = "ar" ; 
        if ($request['lang'] == 'en') 
            $lang = "en" ;
        
        $items = Items::join('products','items.product_id','=','products.id')
                ->get(['items.title_'.$lang.' AS item_title',
                        'items.describtion_'.$lang.' AS item_description',
                        'items.img_url AS item_image_url','items.price',
                        'products.title_'.$lang.' AS product_title',
                        'products.id AS product_id']);        
        return response()->json(['result'=>$items]) ;
    }

    public function get_contact_us(Request $request)
    {
        $lang = "ar" ; 
        if ($request['lang'] == 'en') 
            $lang = "en" ;
        
        $contact_us = CompanyBranches::get(['address_'.$lang.' AS address','phone_one','phone_two']);        
        return response()->json(['result'=>$contact_us]) ;
    }

}
