@extends('dashboard.masterAdmin')
@section('admin')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
										<h2>Products</h2>
										<hr>
@if($flash = session('message'))
	<div class="alert alert-warning" role="alert">
		<b>{{ $flash }}</b>
	</div>	
@endif
	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
		<table class="table table-hover">
	<tr>
		<th>id</th>
		<th>Category title</th>
		<th>Title en</th>
		<th>Title ar</th>
		<th>Describtion en</th>
		<th>Describtion ar</th>
		<th>Image</th>
		<th>Delete</th>
		<th>Edit</th>
	</tr>
	@foreach($products as $product)
	<tr>
		<td>{{ $product->id }}</td>
		<td>{{ $product->category->title_en }}</td>
		<td>{{ $product->title_en }}</td>
		<td>{{ $product->title_ar }}</td>
		<td>{{ $product->describtion_en }}</td>
		<td>{{ $product->describtion_ar }}</td>
		<td><img src="/uploads/{{ $product->img_url }}" width="70"></td>
		<td>
			<a href="/productsEditForm/{{ $product->id }}"><button class="btn btn-success">Edit</button></a>
		</td>
		<td>
		<a href="/productsDelete/{{ $product->id }}"><button class="btn btn-danger">Delete</button></a>
		</td>
	</tr>
	@endforeach
</table>
</div>
</main>
@endsection