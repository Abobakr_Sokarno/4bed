@extends('dashboard.masterAdmin')
@section('admin')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
	<h2>Add About Us</h2>
	<hr>
@if($flash = session('message'))
  <div class="alert alert-warning" role="alert">
    <b>{{ $flash }}</b>
  </div>  
@endif
	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
		<div class="container">
  <form method="POST" action="/about_us">
  	{{ csrf_field() }}
    <div class="form-group row">
      <label for="name" class="col-sm-2 col-form-label">About Us In Arabic</label>
      <div class="col-sm-10">
        <input type="text" name='about_ar' class="form-control">
      </div>
    </div>
    <div class="form-group row">
      <label for="describtion" class="col-sm-2 col-form-label">About Us In English</label>
      <div class="col-sm-10">
        <input type="text" name='about_en' class="form-control">
      </div>
    </div>
     <div class="form-group row">
      <div class="offset-sm-2 col-sm-10">
        <button type="submit" class="btn btn-success">Add About Us</button>
      </div>
    </div>
    @include("errors.errors")
  </form>
</div>
	</div>
</main>
@endsection