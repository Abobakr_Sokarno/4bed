@extends('dashboard.masterAdmin')
@section('admin')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
										<h2>About Us</h2>
										<hr>
@if($flash = session('message'))
	<div class="alert alert-warning" role="alert">
		<b>{{ $flash }}</b>
	</div>	
@endif
	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
		<table class="table table-hover">
	<tr>
		<th>id</th>
		<th>About US in English</th>
		<th>About Us In Arabic</th>
		<th>Delete</th>
		<th>Edit</th>
	</tr>
	@foreach($about_us as $about)
	<tr>
		<td>{{ $about->id }}</td>
		<td>{{ $about->about_en }}</td>
		<td>{{ $about->about_ar }}</td>
		<td>
			<a href="/about_usEditForm/{{ $about->id }}"><button class="btn btn-success">Edit</button></a>
		</td>
		<td>
		<a href="/about_usDelete/{{ $about->id }}"><button class="btn btn-danger">Delete</button></a>
		</td>
	</tr>
	@endforeach
</table>
</div>
</main>
@endsection