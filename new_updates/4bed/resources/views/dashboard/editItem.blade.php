@extends('dashboard.masterAdmin')
@section('admin')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
	<h2>Edit Item</h2>
	<hr>

	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
		<div class="container">
  <form method="POST" action="/items/{{ $item->id }}">
  	{{ csrf_field() }}
  	<input type="hidden" name="_method" value="PATCH">
    <div class="form-group row">
      <label for="name" class="col-sm-2 col-form-label">Item Name In Arabic</label>
      <div class="col-sm-10">
        <input type="text" name='title_ar' class="form-control"  value="{{ $item->title_ar }}">
      </div>
    </div>
    <div class="form-group row">
      <label for="describtion" class="col-sm-2 col-form-label">Item Name In English</label>
      <div class="col-sm-10">
        <input type="text" name='title_en' class="form-control"  value="{{ $item->title_en }}">
      </div>
    </div>
    <div class="form-group row">
      <label for="name" class="col-sm-2 col-form-label">Item Describtion In Arabic</label>
      <div class="col-sm-10">
        <textarea name="describtion_ar" id="editor1" rows="10" cols="80">
           {{ $item->describtion_ar }}     
        </textarea>
      </div>
    </div>
    <div class="form-group row">
      <label for="describtion" class="col-sm-2 col-form-label">Item Describtion In English</label>
      <div class="col-sm-10">
         <textarea name="describtion_en" id="editor2" rows="10" cols="80">
           {{ $item->describtion_en }}     
        </textarea>
      </div>
    </div>
    <div class="form-group row">
      <label for="describtion" class="col-sm-2 col-form-label">Price</label>
      <div class="col-sm-10">
        <input type="text" name='price' class="form-control" value="{{ $item->price }}">
      </div>
    </div>
    <div class="form-group row">
      <label for="category" class="col-sm-2 col-form-label">Choose Product</label>
      <div class="col-sm-10">
        <select name="product_id">
          @foreach($products as $product)
            <option value="{{ $product->id }}" 
            @if ($item->product_id === $product->id)
              {{ "selected" }}
            @endif>{{ $product->title_en }}
            </option>
          @endforeach
        </select>
      </div>
    </div>
     <div class="form-group row">
      <div class="offset-sm-2 col-sm-10">
        <button type="submit" class="btn btn-success">Edit Item</button>
      </div>
    </div>
    @include("errors.errors")
  </form>
</div>
	</div>
</main>
<script>
      CKEDITOR.replace( 'editor1' );
      CKEDITOR.replace( 'editor2' );
</script>
@endsection