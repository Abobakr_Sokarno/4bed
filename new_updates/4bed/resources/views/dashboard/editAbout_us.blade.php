@extends('dashboard.masterAdmin')
@section('admin')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
	<h2>Edit About Us</h2>
	<hr>

	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
		<div class="container">
  <form method="POST" action="/about_us/{{ $about_us->id }}">
  	{{ csrf_field() }}
  	<input type="hidden" name="_method" value="PATCH">
    <div class="form-group row">
      <label for="name" class="col-sm-2 col-form-label">About Us In Arabic</label>
      <div class="col-sm-10">
        <input type="text" name='about_ar' class="form-control"  value="{{ $about_us->about_ar }}">
      </div>
    </div>
    <div class="form-group row">
      <label for="describtion" class="col-sm-2 col-form-label">About Us In English</label>
      <div class="col-sm-10">
        <input type="text" name='about_en' class="form-control"  value="{{ $about_us->about_en }}">
      </div>
    </div>
     <div class="form-group row">
      <div class="offset-sm-2 col-sm-10">
        <button type="submit" class="btn btn-success">Edit About Us</button>
      </div>
    </div>
    @include("errors.errors")
  </form>
</div>
	</div>
</main>
@endsection