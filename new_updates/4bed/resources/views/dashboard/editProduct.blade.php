@extends('dashboard.masterAdmin')
@section('admin')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
	<h2>Edit Product</h2>
	<hr>

	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
		<div class="container">
  <form method="POST" action="/products/{{ $product->id }}">
  	{{ csrf_field() }}
  	<input type="hidden" name="_method" value="PATCH">
    <div class="form-group row">
      <label for="name" class="col-sm-2 col-form-label">Product Name In Arabic</label>
      <div class="col-sm-10">
        <input type="text" name='title_ar' class="form-control"  value="{{ $product->title_ar }}">
      </div>
    </div>
    <div class="form-group row">
      <label for="describtion" class="col-sm-2 col-form-label">Product Name In English</label>
      <div class="col-sm-10">
        <input type="text" name='title_en' class="form-control"  value="{{ $product->title_en }}">
      </div>
    </div>
    <div class="form-group row">
      <label for="name" class="col-sm-2 col-form-label">Product Describtion In Arabic</label>
      <div class="col-sm-10">
        <textarea name="describtion_ar" id="editor1" rows="10" cols="80">
           {{ $product->describtion_ar }}     
        </textarea>
      </div>
    </div>
    <div class="form-group row">
      <label for="describtion" class="col-sm-2 col-form-label">Product Describtion In English</label>
      <div class="col-sm-10">
         <textarea name="describtion_en" id="editor2" rows="10" cols="80">
           {{ $product->describtion_en }}     
        </textarea>
      </div>
    </div>
    <div class="form-group row">
      <label for="category" class="col-sm-2 col-form-label">Choose Category</label>
      <div class="col-sm-10">
        <select name="category_id">
          @foreach($cats as $cat)
            <option value="{{ $cat->id }}" 
            @if ($product->category_id === $cat->id)
              {{ "selected" }}
            @endif>{{ $cat->title_en }}
            </option>
          @endforeach
        </select>
      </div>
    </div>
     <div class="form-group row">
      <div class="offset-sm-2 col-sm-10">
        <button type="submit" class="btn btn-success">Edit Product</button>
      </div>
    </div>
    @include("errors.errors")
  </form>
</div>
	</div>
</main>
<script>
      CKEDITOR.replace( 'editor1' );
      CKEDITOR.replace( 'editor2' );
</script>
@endsection