$(".like").on("click", function(){

	var like_s = $(this).attr('data-like');
	var articles_id = $(this).attr('data-articlesid');
	articles_id = articles_id.slice(0, -2);
	// alert(like_s); for testing code

	$.ajax({
		type: 'POST',
		url: url,
		data: {like_s: like_s, articles_id: articles_id, _token: token},

		success: function (data) {

			// alert(data.is_like)
			if (data.is_like == 1) 
			{
				$('*[data-articlesid="'+ articles_id +'_l"]').removeClass('btn-secondry').addClass('btn-info');
				$('*[data-articlesid="'+ articles_id +'_d"]').removeClass('btn-danger').addClass('btn-secondry');
				//For Counter
				var cu_like = $('*[data-articlesid="'+ articles_id +'_l"]').find('.like_count').text();
				var new_like = parseInt(cu_like) + 1;
				$('*[data-articlesid="'+ articles_id +'_l"]').find('.like_count').text(new_like);
				//to minuse 1 if changed status from dislike to like
				if (data.change_like == 1) 
				{
					var cu_dislike = $('*[data-articlesid="'+ articles_id +'_d"]').find('.dislike_count').text();
					var new_dislike = parseInt(cu_dislike) - 1;
					$('*[data-articlesid="'+ articles_id +'_d"]').find('.dislike_count').text(new_dislike);
				}
			}

			if (data.is_like == 0) 
			{
				$('*[data-articlesid="'+ articles_id +'_l"]').removeClass('btn-info').addClass('btn-secondry');
				//for removing count
				var cu_like = $('*[data-articlesid="'+ articles_id +'_l"]').find('.like_count').text();
				var new_like = parseInt(cu_like) - 1;
				$('*[data-articlesid="'+ articles_id +'_l"]').find('.like_count').text(new_like);
			}
		}
	})

});

$(".dislike").on("click", function(){

	var like_s = $(this).attr('data-like');
	var articles_id = $(this).attr('data-articlesid');
	articles_id = articles_id.slice(0, -2);
	// alert(like_s); for testing code

	$.ajax({
		type: 'POST',
		url: url_dis,
		data: {like_s: like_s, articles_id: articles_id, _token: token},

		success: function (data) {

			// alert(data.is_like)
			if (data.is_dislike == 1) 
			{
				$('*[data-articlesid="'+ articles_id +'_d"]').removeClass('btn-secondry').addClass('btn-danger');
				$('*[data-articlesid="'+ articles_id +'_l"]').removeClass('btn-info').addClass('btn-secondry');			
				//For Counter
				var cu_dislike = $('*[data-articlesid="'+ articles_id +'_d"]').find('.dislike_count').text();
				var new_dislike = parseInt(cu_dislike) + 1;
				$('*[data-articlesid="'+ articles_id +'_d"]').find('.dislike_count').text(new_dislike);
			
				//to minuse 1 if changed status from like to dislike
				if (data.change_dislike == 1) 
				{
					var cu_like = $('*[data-articlesid="'+ articles_id +'_l"]').find('.like_count').text();
					var new_like = parseInt(cu_like) - 1;
					$('*[data-articlesid="'+ articles_id +'_l"]').find('.like_count').text(new_like);
				}
			}

			if (data.is_dislike == 0) 
			{
				$('*[data-articlesid="'+ articles_id +'_d"]').removeClass('btn-danger').addClass('btn-secondry');
				//For remove Count
				var cu_dislike = $('*[data-articlesid="'+ articles_id +'_d"]').find('.dislike_count').text();
				var new_dislike = parseInt(cu_dislike) - 1;
				$('*[data-articlesid="'+ articles_id +'_d"]').find('.dislike_count').text(new_dislike);
			}
		}
	})

});