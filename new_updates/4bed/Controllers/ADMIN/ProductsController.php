<?php

namespace App\Http\Controllers\ADMIN;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Category;

class ProductsController extends Controller
{
    public function index()
    {
        $products = Product::all();

        return view('dashboard.products', compact('products'));
    }

    public function show_add_form()
    {
    	$cats=Category::all();
    	return view('dashboard.addProduct', compact('cats'));
    }

    public function show($id)
    {
    	$cats=Category::all();
        $product = Product::find($id);
        return view('dashboard.editProduct', compact('product', 'cats'));
    }

    public function store(Request $request)
    {	
    	$this->validate(request(),[
            'title_ar'=>'required',
            'title_en'=>'required',
            'describtion_ar'=>'required',
            'describtion_en'=>'required',
            'img_url'=>'required|image|mimes:jpg,jpeg,png,gif|max:2048',
            'category_id'=>'required'
        ]);

        $img_name=time() . '.' . $request->img_url->getClientOriginalExtension();

        $product= new Product;
        $product->title_ar=request('title_ar');
        $product->title_en=request('title_en');
        $product->describtion_ar=request('describtion_ar');
        $product->describtion_en=request('describtion_en');
        $product->img_url=$img_name;
        $product->category_id=request('category_id');
        $product->save();

        $request->img_url->move(public_path('uploads'), $img_name);

        session()->flash('message', 'Product is Created');

        return redirect('createProduct');
    }

    public function update(Request $request, $id)
    {
    	$this->validate(request(),[
            'title_ar'=>'required',
            'title_en'=>'required',
            'describtion_ar'=>'required',
            'describtion_en'=>'required',
            'category_id'=>'required'
        ]);

        $product=new Product;
        $product->where('id', $id)
        	 	  ->update(['title_ar'=>request('title_ar'), 'title_en'=>request('title_en'), 'describtion_ar'=>request('describtion_ar'), 'describtion_en'=>request('describtion_en'), 'category_id' => request('category_id')]);
       	session()->flash('message', 'Product is Updated');

        return redirect('/products');
    }

    public function delete($id)
    {
        $product = Product::findOrFail($id);
        $product->delete();

        session()->flash('message', 'Product is Deleted');

        return back();
    }
}
