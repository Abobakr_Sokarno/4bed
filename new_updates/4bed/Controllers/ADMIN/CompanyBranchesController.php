<?php

namespace App\Http\Controllers\ADMIN;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\CompanyBranches;

class CompanyBranchesController extends Controller
{
    public function index()
    {
        $company_branches = CompanyBranches::all();

        return view('dashboard.company_branches', compact('company_branches'));
    }

    public function show_add_form()
    {
    	return view('dashboard.addCompany_branch');
    }

    public function show($id)
    {
        $company_branch = CompanyBranches::find($id);
        return view('dashboard.editCompany_branch', compact('company_branch'));
    }

    public function store(Request $request)
    {	
    	$this->validate(request(),[
            'address_en'=>'required',
            'address_ar'=>'required',
            'phone_one'=>'required',
            'phone_two'=>'required',
        ]);

        $company_branch = CompanyBranches::create($request->all());
        session()->flash('message', 'Company Branch is Created');

        return redirect('createCompany_branch');
    }

    public function update(Request $request, $id)
    {
    	$this->validate(request(),[
            'address_en'=>'required',
            'address_ar'=>'required',
            'phone_one'=>'required',
            'phone_two'=>'required',
        ]);

        $company_branch=new CompanyBranches;
        $company_branch->where('id', $id)
        	 	  ->update(['address_en'=>request('address_en'), 'address_ar'=>request('address_ar'),'phone_one'=>request('phone_one'), 'phone_two'=>request('phone_two')]);
       	session()->flash('message', 'Company Branch is Updated');

        return redirect('/company_branches');
    }

    public function delete($id)
    {
        $company_branch = CompanyBranches::findOrFail($id);
        $company_branch->delete();

        session()->flash('message', 'Company Branch is Deleted');

        return back();
    }
}
