<?php

namespace App\Http\Controllers\ADMIN;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\About;

class AboutUsController extends Controller
{
    public function index()
    {
        $about_us = About::all();

        return view('dashboard.about_us', compact('about_us'));
    }

    public function show_add_form()
    {
    	return view('dashboard.addAbout_us');
    }

    public function show($id)
    {
        $about_us = About::find($id);
        return view('dashboard.editAbout_us', compact('about_us'));
    }

    public function store(Request $request)
    {	
    	$this->validate(request(),[
            'about_en'=>'required',
            'about_ar'=>'required',
        ]);

        $about_us = About::create($request->all());
        session()->flash('message', 'Company Branch is Created');

        return redirect('createAbout_us');
    }

    public function update(Request $request, $id)
    {
    	$this->validate(request(),[
            'about_en'=>'required',
            'about_ar'=>'required',
        ]);

        $about_us=new About;
        $about_us->where('id', $id)
        	 	  ->update(['about_ar'=>request('about_ar'), 'about_en'=>request('about_en')]);

       	session()->flash('message', 'About Us is Updated');

        return redirect('/about_us');
    }

    public function delete($id)
    {
        $about_us = About::findOrFail($id);
        $about_us->delete();

        session()->flash('message', 'About Us is Deleted');

        return back();
    }
}
