<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Product;

class Items extends Model
{
     protected $fillable = ['title)_en', 'title_ar', 'describtion_en', 'describtion_ar','price', 'product_id', 'img_url' ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
