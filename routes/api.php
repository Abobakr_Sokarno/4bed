<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('about_us_info','APIController@about_us');
// Route::get('/company_branches', 'APIController@company_branches');
Route::get('/categories', 'APIController@get_all_categories');
Route::get('/get_product/{id}','APIController@get_product') ;
Route::get('/get_products_by_category/{category_id}', 'APIController@get_products_by_category');
Route::get('/get_items_by_product/{product_id}', 'APIController@get_items_by_product');
Route::get('/get_all_products' , 'APIController@get_all_products');
Route::get('/get_all_items', 'APIController@get_all_items');
Route::get('/contact_us_info','APIController@get_contact_us') ;


