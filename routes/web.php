<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('dashboard.admin_panel');
});


//Admin Controls - categories
Route::get('/categories', 'ADMIN\CategoriesController@index');
Route::get('/categoriesEditForm/{id}', 'ADMIN\CategoriesController@show');
Route::get('/createCategory', 'ADMIN\CategoriesController@show_add_form');
Route::post('/categories', 'ADMIN\CategoriesController@store');
Route::patch('/categories/{id}', 'ADMIN\CategoriesController@update');
Route::get('/categoriesDelete/{id}', 'ADMIN\CategoriesController@delete');

//Admin Controls - products
Route::get('/products', 'ADMIN\ProductsController@index');
Route::get('/productsEditForm/{id}', 'ADMIN\ProductsController@show');
Route::get('/createProduct', 'ADMIN\ProductsController@show_add_form');
Route::post('/products', 'ADMIN\ProductsController@store');
Route::patch('/products/{id}', 'ADMIN\ProductsController@update');
Route::get('/productsDelete/{id}', 'ADMIN\ProductsController@delete');

//Admin Controls - items
Route::get('/items', 'ADMIN\ItemsController@index');
Route::get('/itemsEditForm/{id}', 'ADMIN\ItemsController@show');
Route::get('/createItem', 'ADMIN\ItemsController@show_add_form');
Route::post('/items', 'ADMIN\ItemsController@store');
Route::patch('/items/{id}', 'ADMIN\ItemsController@update');
Route::get('/itemsDelete/{id}', 'ADMIN\ItemsController@delete');

//Admin Controls - Company Branches
Route::get('/company_branches', 'ADMIN\CompanyBranchesController@index');
Route::get('/company_branchesEditForm/{id}', 'ADMIN\CompanyBranchesController@show');
Route::get('/createCompany_branch', 'ADMIN\CompanyBranchesController@show_add_form');
Route::post('/company_branches', 'ADMIN\CompanyBranchesController@store');
Route::patch('/company_branches/{id}', 'ADMIN\CompanyBranchesController@update');
Route::get('/company_branchesDelete/{id}', 'ADMIN\CompanyBranchesController@delete');

//Admin Controls - About Us
Route::get('/about_us', 'ADMIN\AboutUsController@index');
Route::get('/about_usEditForm/{id}', 'ADMIN\AboutUsController@show');
Route::get('/createAbout_us', 'ADMIN\AboutUsController@show_add_form');
Route::post('/about_us', 'ADMIN\AboutUsController@store');
Route::patch('/about_us/{id}', 'ADMIN\AboutUsController@update');
Route::get('/about_usDelete/{id}', 'ADMIN\AboutUsController@delete');


