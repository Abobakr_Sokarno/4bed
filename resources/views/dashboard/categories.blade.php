@extends('dashboard.masterAdmin')
@section('admin')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
										<h2>Categories</h2>
										<hr>
@if($flash = session('message'))
	<div class="alert alert-warning" role="alert">
		<b>{{ $flash }}</b>
	</div>	
@endif
	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
		<table class="table table-hover">
	<tr>
		<th>id</th>
		<th>Title En</th>
		<th>Title Ar</th>
		<th>Delete</th>
		<th>Edit</th>
	</tr>
	@foreach($categories as $category)
	<tr>
		<td>{{ $category->id }}</td>
		<td>{{ $category->title_en }}</td>
		<td>{{ $category->title_ar }}</td>
		<td>
			<a href="{{url('categoriesEditForm/'.$category->id)}}"><button class="btn btn-success">Edit</button></a>
		</td>
		<td>
		<a href="{{url('categoriesDelete/'.$category->id)}}"><button class="btn btn-danger">Delete</button></a>
		</td>
	</tr>
	@endforeach
</table>
</div>
</main>
@endsection