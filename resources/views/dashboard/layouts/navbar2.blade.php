<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Admin Panel</title>

    <!-- Bootstrap core CSS -->
    <link href="{{url('css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{url('css/dashboard.css')}}" rel="stylesheet">
    <script src="https://cdn.ckeditor.com/4.9.2/standard/ckeditor.js"></script>
  </head>

  <body style="margin: -50px auto;">
    <nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
      <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">
        <span data-feather="aperture"></span>&nbsp;4-Bed</a>
      <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
          <a class="nav-link" href="{{url('logout')}}"><button class="btn btn-info"><span data-feather="log-out" style="margin: -5px auto"></span>&nbsp;Sign out</button></a>
        </li>
      </ul>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
          <div class="sidebar-sticky">
            <ul class="nav flex-column">
              <li class="nav-item">
                <a class="nav-link active" href="{{url('/')}}">
                  <span data-feather="home"></span>
                  Dashboard
                </a>
              </li>
              <hr>
              <li class="nav-item">
                <a class="nav-link" href="{{url('categories')}}">
                  <span data-feather="folder"></span>
                  Categories
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{url('createCategory')}}">
                  <span data-feather="folder-plus"></span>
                  Add Category
                </a>
              </li>
              <hr>
              <li class="nav-item">
                <a class="nav-link" href="{{url('products')}}">
                  <span data-feather="file"></span>
                  Products
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{url('createProduct')}}">
                  <span data-feather="file-plus"></span>
                  Add Products 
                </a>
              </li>
              <hr>
              <!-- <li class="nav-item">
                <a class="nav-link" href="{{url('items')}}">
                  <span data-feather="layers"></span>
                  Items
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{url('createItem')}}">
                  <span data-feather="plus-square"></span>
                  Add Items
                </a>
              </li>
              <hr> -->
              <li class="nav-item">
                <a class="nav-link" href="{{url('company_branches')}}">
                  <span data-feather="git-branch"></span>
                  Company Branches
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{url('createCompany_branch')}}">
                  <span data-feather="plus"></span>
                  Add Company Branches
                </a>
              </li>
              <hr>
              <li class="nav-item">
                <a class="nav-link" href="{{url('about_us')}}">
                  <span data-feather="info"></span>
                  About Us
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{url('createAbout_us')}}">
                  <span data-feather="plus"></span>
                  Add About Us
                </a>
              </li>
            </ul>
          </div>
        </nav>
      </div>
    </div>
