@extends('dashboard.masterAdmin')
@section('admin')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
  <h2>Add a Product</h2>
  <hr>
@if($flash = session('message'))
  <div class="alert alert-warning" role="alert">
    <b>{{ $flash }}</b>
  </div>  
@endif
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
    <div class="container">
  <form method="POST" action="{{url('products')}}" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="form-group row">
      <label for="name" class="col-sm-2 col-form-label">Product Name In Arabic</label>
      <div class="col-sm-10">
        <input type="text" name='title_ar' class="form-control"">
      </div>
    </div>
    <div class="form-group row">
      <label for="describtion" class="col-sm-2 col-form-label">Product Name In English</label>
      <div class="col-sm-10">
        <input type="text" name='title_en' class="form-control">
      </div>
    </div>
    <div class="form-group row">
      <label for="name" class="col-sm-2 col-form-label">Product Describtion In Arabic</label>
      <div class="col-sm-10">
        <textarea name="describtion_ar" id="editor1" rows="10" cols="80">
                
        </textarea>
      </div>
    </div>
    <div class="form-group row">
      <label for="describtion" class="col-sm-2 col-form-label">Product Describtion In English</label>
      <div class="col-sm-10">
        <textarea name="describtion_en" id="editor2" rows="10" cols="80">
                
        </textarea>
      </div>
    </div>
    <div class="form-group row">
      <label for="category" class="col-sm-2 col-form-label">Choose Category</label>
      <div class="col-sm-10">
        <select name="category_id">
          <option disabled selected>Choose Category</option>
          @foreach($cats as $cat)
            <option value="{{ $cat->id }}">{{ $cat->title_en }}</option>
          @endforeach
        </select>
      </div>
    </div>
    <div class="form-group">
      <label for="upload">Upload Image</label>
      <input type="file" name="img_url" class="form-control">
    </div>
     <div class="form-group row">
      <div class="offset-sm-2 col-sm-10">
        <button type="submit" class="btn btn-success">Add Product</button>
      </div>
    </div>
    @include("errors.errors")
  </form>
</div>
  </div>
</main>
<script>
      CKEDITOR.replace( 'editor1' );
      CKEDITOR.replace( 'editor2' );
</script>
@endsection