@extends('dashboard.masterAdmin')
@section('admin')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
	<h2>Edit Company Branch</h2>
	<hr>

	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
		<div class="container">
  <form method="POST" action="{{url('company_branches/'.$company_branch->id)}}">
  	{{ csrf_field() }}
  	<input type="hidden" name="_method" value="PATCH">
    <div class="form-group row">
      <label for="name" class="col-sm-2 col-form-label">Address In Arabic</label>
      <div class="col-sm-10">
        <input type="text" name='address_ar' class="form-control"  value="{{ $company_branch->address_ar }}">
      </div>
    </div>
    <div class="form-group row">
      <label for="describtion" class="col-sm-2 col-form-label">Address In English</label>
      <div class="col-sm-10">
        <input type="text" name='address_en' class="form-control"  value="{{ $company_branch->address_ar }}">
      </div>
    </div>
    <div class="form-group row">
      <label for="describtion" class="col-sm-2 col-form-label">First Phone Number</label>
      <div class="col-sm-10">
        <input type="text" name='phone_one' class="form-control" value="{{ $company_branch->phone_one }}">
      </div>
    </div>
    @if(isset($company_branch->phone_two))
    <div class="form-group row">
      <label for="describtion" class="col-sm-2 col-form-label">Second Phone Number</label>
      <div class="col-sm-10">
        <input type="text" name='phone_two' class="form-control" value="{{ $company_branch->phone_two }}">
      </div>
    </div>
    @endif
     <div class="form-group row">
      <div class="offset-sm-2 col-sm-10">
        <button type="submit" class="btn btn-success">Edit Company Branch</button>
      </div>
    </div>
    @include("errors.errors")
  </form>
</div>
	</div>
</main>
@endsection